# snapd

snapd comes pre-installed on Ubuntu 16.04.4 LTS and up, Zorin OS, and most Ubuntu flavours.
https://snapcraft.io/docs/installing-snapd

Other distributions don't usually have snap pre-installed, most importantly for us, Debian.
https://snapcraft.io/docs/installing-snap-on-debian

This role installs snapd, and ensures that our snap version is up-to-date (this works on Debian too):
```
sudo apt install snapd
sudo snap install core
sudo snap refresh core
```

## Ansible collection `community.general.snap`

This should be part of `ansible` if you installed it via pip
or using [our Ansible role](https://codeberg.org/ansible/ansible).
Note that this is needed on the *controller*, not the target,
i.e., on the computer running the Ansible playbook.

Otherwise, in your playbook repo, install the collection:
```
$ ansible-galaxy collection install community.general
```

To use it in a playbook, specify: `community.general.snap`.

+ https://docs.ansible.com/ansible/latest/collections/community/general/snap_module.html
